# PROJECT INFO

**TASK-MANAGER**

# DEVELOPER INFO

**NAME:** Ovechkin Roman

**E-MAIL:** roman@ovechkin.ru

# SOFTWARE

- JDK 1.8
- MS WINDOWS 10

# PROGRAM RUN

```bash
java -jar ./task-manager.jar
```

# SCREENSHOTS

| Описание | Ссылка |
|:----|:----|
| Ввод комманд | https://yadi.sk/i/ycaLkSK3I8xC7Q |
| Ввод аргументов | https://yadi.sk/i/RtpKvYAK2p27UQ | 