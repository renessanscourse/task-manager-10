package ru.ovechkin.tm.api;

public interface ICommandController {

    void displayWelcome();

    void showVersion();

    void showAbout();

    void showCommands();

    void showArguments();

    void showHelp();

    void showInfo();

    void exit();


}
