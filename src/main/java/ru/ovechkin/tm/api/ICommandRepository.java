package ru.ovechkin.tm.api;

import ru.ovechkin.tm.model.TerminalCommand;

public interface ICommandRepository {

    String[] getCommands(TerminalCommand... values);

    String[] getArgs(TerminalCommand... values);

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();

}