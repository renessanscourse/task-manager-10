package ru.ovechkin.tm.api;

import ru.ovechkin.tm.model.TerminalCommand;

public interface ICommandService {

    TerminalCommand[] getTerminalCommands();

    String[] getCommands();

    String[] getArgs();
}
