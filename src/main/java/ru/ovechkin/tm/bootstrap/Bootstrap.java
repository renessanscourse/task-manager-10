package ru.ovechkin.tm.bootstrap;

import ru.ovechkin.tm.api.ICommandController;
import ru.ovechkin.tm.api.ICommandService;
import ru.ovechkin.tm.constant.IArgumentConst;
import ru.ovechkin.tm.constant.ICmdConst;
import ru.ovechkin.tm.controller.CommandController;
import ru.ovechkin.tm.repository.CommandRepository;
import ru.ovechkin.tm.service.CommandService;

import java.util.Scanner;

public final class Bootstrap {

    private final CommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) {
        commandController.displayWelcome();
        if (parseArgs(args)) System.exit(0);
        process();
    }

    private void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!ICmdConst.CMD_EXIT.equals(command)) {
            System.out.print("Enter command: ");
            command = scanner.nextLine();
            parseCommand(command);
            parseArg(command);
            System.out.println();
        }
    }

    private boolean parseArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        parseCommand(arg);
        return true;
    }

    private void parseArg(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case IArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case IArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case IArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case IArgumentConst.ARG_INFO:
                commandController.showInfo();
                break;
            case IArgumentConst.ARG_ARGUMENTS:
                commandController.showArguments();
                break;
            case IArgumentConst.ARG_COMMANDS:
                commandController.showCommands();
                break;
        }
    }

    private void parseCommand(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ICmdConst.CMD_VERSION:
                commandController.showVersion();
                break;
            case ICmdConst.CMD_ABOUT:
                commandController.showAbout();
                break;
            case ICmdConst.CMD_HELP:
                commandController.showHelp();
                break;
            case ICmdConst.CMD_INFO:
                commandController.showInfo();
                break;
            case ICmdConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case ICmdConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case ICmdConst.CMD_EXIT:
                commandController.exit();
                break;
        }
    }

}
