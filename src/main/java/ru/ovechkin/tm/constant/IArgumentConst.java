package ru.ovechkin.tm.constant;

public interface IArgumentConst {

    String ARG_HELP = "-h";

    String ARG_INFO = "-i";

    String ARG_VERSION = "-v";

    String ARG_ABOUT = "-a";

    String ARG_ARGUMENTS = "-ar";

    String ARG_COMMANDS = "-c";

}
