package ru.ovechkin.tm.constant;

public interface ICmdConst {

    String CMD_HELP = "help";

    String CMD_VERSION = "version";

    String CMD_ABOUT = "about";

    String CMD_EXIT = "exit";

    String CMD_INFO = "info";

    String CMD_ARGUMENTS = "arguments";

    String CMD_COMMANDS = "commands";

}
