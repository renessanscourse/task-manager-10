package ru.ovechkin.tm.repository;

import ru.ovechkin.tm.api.ICommandRepository;
import ru.ovechkin.tm.constant.IArgumentConst;
import ru.ovechkin.tm.constant.ICmdConst;
import ru.ovechkin.tm.model.TerminalCommand;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    public static final TerminalCommand HELP = new TerminalCommand(
            ICmdConst.CMD_HELP, IArgumentConst.ARG_HELP, " Display terminal commands"
    );

    public static final TerminalCommand ABOUT = new TerminalCommand(
            ICmdConst.CMD_ABOUT, IArgumentConst.ARG_ABOUT, " Show developer info"
    );

    public static final TerminalCommand VERSION = new TerminalCommand(
            ICmdConst.CMD_VERSION, IArgumentConst.ARG_VERSION, " Show version info"
    );

    public static final TerminalCommand INFO = new TerminalCommand(
            ICmdConst.CMD_INFO, IArgumentConst.ARG_INFO, " Display system's info"
    );

    public static final TerminalCommand EXIT = new TerminalCommand(
            ICmdConst.CMD_EXIT, null, " Close application"
    );

    public static final TerminalCommand HELP_COMMANDS = new TerminalCommand(
            ICmdConst.CMD_COMMANDS, IArgumentConst.ARG_COMMANDS, " Show available commands"
    );

    public static final TerminalCommand HELP_ARGUMENTS = new TerminalCommand(
            ICmdConst.CMD_ARGUMENTS, IArgumentConst.ARG_ARGUMENTS, " Show available arguments"
    );

    private final TerminalCommand[] TERMINAL_COMMANDS = new TerminalCommand[]{
            HELP, ABOUT, VERSION, INFO, EXIT, HELP_COMMANDS, HELP_ARGUMENTS
    };



    private final String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private final String[] ARGS = getArgs(TERMINAL_COMMANDS);

    public String[] getCommands(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArgs(TerminalCommand... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public TerminalCommand[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArgs() {
        return ARGS;
    }

}
