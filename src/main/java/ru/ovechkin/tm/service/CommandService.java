package ru.ovechkin.tm.service;

import ru.ovechkin.tm.api.ICommandService;
import ru.ovechkin.tm.model.TerminalCommand;
import ru.ovechkin.tm.repository.CommandRepository;

public class CommandService implements ICommandService {

    private CommandRepository commandRepository;

    public CommandService(CommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public TerminalCommand[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

    public String[] getCommands() {
        return commandRepository.getCommands();
    }

    public String[] getArgs() {
        return commandRepository.getArgs();
    }
}
